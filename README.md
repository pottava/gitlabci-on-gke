# GitLab CI on GKE

## 基本的な設定

### Google Cloud services

```sh
gcloud services enable compute.googleapis.com container.googleapis.com
```

### GitLab のインストール（Option）

VM に設定するサービス アカウントを作成します。

```sh
export project_id=$( gcloud config get-value project )
gcloud iam service-accounts create vm-gitlab --display-name "SA for GitLab VMs" \
    --description "Service Account for the GitLab machines"
```

VM を起動します。

```sh
gcloud compute instances create gitlab --zone "asia-northeast1-c" \
    --machine-type "n2-standard-2" --network-interface "network-tier=PREMIUM,subnet=default" \
    --service-account "vm-gitlab@${project_id}.iam.gserviceaccount.com" \
    --scopes "https://www.googleapis.com/auth/cloud-platform" \
    --metadata "enable-oslogin=true"
```

SSH over IAP で VM に入り、GitLab をインストールします。

```sh
gcloud compute firewall-rules create allow-ssh-from-iap --direction INGRESS \
    --action allow --rules "tcp:22" --source-ranges "35.235.240.0/20"
gcloud compute ssh gitlab --zone "asia-northeast1-c" --tunnel-through-iap
# Follow the instructions on https://about.gitlab.com/install/#debian, then
sudo cat /etc/gitlab/initial_root_password
```

VM 外部から GitLab へアクセスしてみます。

```sh
gcloud compute firewall-rules create allow-http --direction INGRESS \
    --action allow --rules "tcp:80" --source-ranges "0.0.0.0/0"
echo "http://$( gcloud compute instances describe gitlab --zone asia-northeast1-c \
  --format 'get(networkInterfaces[0].accessConfigs[0].natIP)' )/"
```

### GKE Autopilot クラスタの起動

CI ランナーのためのクラスタを起動します。

```sh
gcloud container clusters create-auto gitlab-ci --region asia-northeast1 \
    --release-channel stable
```

### CI ランナーのインストール（プロジェクト固有）

GitLab CI を Kubernetes 上で動かす設定はインターネット上さまざま見つかりますが、v15.8 時点においては Kubernetes 側に Runner のエージェントを起動するだけで CI が動き始めます。まずはプロジェクト固有のランナーを設定してみましょう。CI を設定する GitLab プロジェクトの "Settings > CI/CD" ページの "Runners" 設定を開き、Registration Token を確認、変数に設定します。

```sh
export registration_token=
```

CI ランナーをインストールします。

```sh
helm repo add gitlab https://charts.gitlab.io
helm repo update gitlab
kubectl create ns gitlab-runner
cat << EOF > runner.yaml
gitlabUrl: http://$( gcloud compute instances describe gitlab --zone asia-northeast1-c \
  --format 'get(networkInterfaces[0].accessConfigs[0].natIP)' )/
runnerRegistrationToken: "${registration_token}"
rbac:
  create: true
runners:
  image: ubuntu:18.04
  privileged: false
EOF
helm install --namespace gitlab-runner gitlab-runner -f runner.yaml gitlab/gitlab-runner
kubectl wait -n gitlab-runner --for=condition=Ready pod --all
```

## 応用編

[CI ランナーの設定ファイル](https://docs.gitlab.com/runner/install/kubernetes.html)を参考に、ランナーの設定を変更することができます。

### キャッシュを設定する

前のジョブで利用したコンテンツを再利用しジョブの実行時間を短縮することを目的に、キャッシュを導入します。まずキャッシュ用のバケットを GCS に作成します。

```sh
gcloud storage buckets create "gs://gitlab-cache-${project_id}" --location "asia-northeast1" \
    --uniform-bucket-level-access --public-access-prevention --enable-autoclass
```

このバケットにのみ書き込み権限をもつサービスアカウントと、その鍵を作成します。

```sh
gcloud iam service-accounts create gitlab-cache --display-name "SA for GitLab CI Cache" \
    --description "Service Account for the GitLab CI Cache"
roles="objectCreator,objectViewer"
gsutil iam ch "serviceAccount:gitlab-cache@${project_id}.iam.gserviceaccount.com:${roles}" \
    "gs://gitlab-cache-${project_id}"
gcloud iam service-accounts keys create key.json \
    --iam-account "gitlab-cache@${project_id}.iam.gserviceaccount.com"
kubectl create secret generic google-application-credentials \
    --from-file "gcs-application-credentials-file=key.json"
rm -rf key.json
```

Job の間でデータがやりとりできるよう、キャッシュを設定します。

```sh
cat << EOF > runner.yaml
gitlabUrl: http://$( gcloud compute instances describe gitlab --zone asia-northeast1-c \
  --format 'get(networkInterfaces[0].accessConfigs[0].natIP)' )/
runnerRegistrationToken: "${registration_token}"
rbac:
  create: true
runners:
  image: ubuntu:18.04
  privileged: false
  cache:
    cacheType: gcs
    cacheShared: true
    gcsBucketName: gitlab-cache-${project_id}
    secretName: google-application-credentials
secrets:
  - name: google-application-credentials
EOF
helm upgrade --namespace gitlab-runner gitlab-runner -f runner.yaml gitlab/gitlab-runner
kubectl wait -n gitlab-runner --for=condition=Ready pod --all
```
